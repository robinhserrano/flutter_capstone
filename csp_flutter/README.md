# Flutter Capstone

## Setup Guide

This section focuses on the Step-by-step instruction to clone the project locally and install the needed Flutter packages.

1. Open GitBash or Terminal in your computer.
2. Change the current working directory to the location where you want the cloned directory.
3. Type `git clone`, and then paste the URL you copied earlier. `git clone <project_url>`.
4. Navigate first to the API folder by using the command cd `csp_api`.
5. Type `dart bin/main.dart` in the terminal to run the app's API.
6. Navigate back to the project's root folder by using the `cd ..` command.
7. Navigate to the flutter project by using the command `cd csp_flutter`.
8. Type `flutter pub get` to install the required packages in `pubspec.yaml` to your device.
9. Type `flutter run` to start building or running the cloned flutter application. (Note: Dart and Flutter SDK must be already installed in your computer, otherwise follow the instructions in the provided [link](https://flutter.dev/docs/get-started/install/windows)).

# Installed Packages

The following are the packages installed in pubspec.yaml with their respective description: 

- **[animated_text_kit](https://pub.dev/packages/http) 4.2.1**- is a flutter package project which contains a collection of cool and beautiful text animations.
- **[carousel_slider](https://pub.dev/packages/carousel_slider) 4.0.0**- is a carousel slider widget, support infinite scroll and custom child widget.
- **[flutter_dotenv](https://pub.dev/packages/flutter_dotenv) 5.0.0**- used to easily configure any flutter application with global variables using a `.env` file.
- **[flutter_mobx](https://pub.dev/packages/flutter_mobx) 2.0.2**- is a Flutter integration for MobX. It provides a set of Observer widgets that automatically rebuild when the tracked observables change.
- **[font_awesome_flutter](https://pub.dev/packages/font_awesome_flutter) 9.1.0**- The Font Awesome Icon pack available as Flutter Icons. Provides 1500 additional icons to use in your apps
- **[google_fonts](https://pub.dev/packages/google_fonts) 2.1.0**- a package to include fonts from [fonts.google.com](http://fonts.google.com/) in your Flutter app.\
- **[http](https://pub.dev/packages/http) ^0.13.3**- contains a set of high-level functions and classes that make it easy to consume HTTP resources. It's multi-platform, and supports mobile, desktop, and the browser.
- **[image_picker](https://pub.dev/packages/image_picker) 0.8.0+3**- Flutter plugin for selecting images from the Android and iOS image library, and taking new pictures with the camera.
- **[provider](https://pub.dev/packages/provider) 5.0.0**- is a wrapper around InheritedWidget to make them easier to use and more reusable.
- **[shared_preferences](https://pub.dev/packages/shared_preferences) 2.0.6**- is a Flutter plugin for reading and writing simple key-value pairs. Wraps NSUserDefaults on iOS and SharedPreferences on Android.
- **[sleek_circular_slider](https://pub.dev/packages/sleek_circular_slider) 2.0.1**- is highly customizable circular slider/progress bar & spinner for Flutter.
- **[url_launcher](https://pub.dev/packages/url_launcher) 6.0.9**- Flutter plugin for launching a URL. Supports web, phone, SMS, and email schemes.

## Flutter Version

The flutter version can be seen from pubspec.yaml which was set to ">=2.12.0 <3.0.0". 

The most important note in the version used was the introduction of Flutter null safety which is a feature that was made accessible following the release of version 2 of the Dart programming language, which has a minimum version of 2.12.

## App Features

### Login Feature

- Save token to app state
- Save token to local storage

The project management app recognizes only 3 types of Job Position namely: Contractor, Subcontractor, and Assembly Team, each of them has their unique access to the app's features. 

[Test User Credentials](https://www.notion.so/6d88e1c95a6d45b2b239aa1cfdd955f7)

### Contractor

- Show all projects
- Add project
- Assign project to subcontractor

### Subcontractor

- Show assigned projects
- Show project tasks
- Add task & assign to assembly team

### Assembly Team

- Show projects with assigned tasks
- Show projects tasks
- Tag Task as ongoing

### List of Screens in the App

- Login Screen - the screen allows you to login inside the app with an authorized email and password.
- Project List Screen - this screen shows the current list of projects, also allows a contractor to assign a project to subcontractor.
- Task List Screen - this screen shows the tasks assigned to an assembly team.
- Task Detail Screen - this screen shows the description of the task given to the assembly team.
- About Screen - this screen allows the user to view the app creator's portfolio accessible through the app drawer.