import 'package:csp_flutter/utils/api.dart';
import 'package:csp_flutter/utils/functions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/models/task.dart';
import '/providers/user_provider.dart';
import '/screens/task_detail_screen.dart';

class TaskCard extends StatefulWidget {
    final Task _task;
    final Function _reloadTasks;

    TaskCard(this._task, this._reloadTasks);

    @override
    _TaskCard createState() => _TaskCard();
}
class _TaskCard extends State<TaskCard> {    
    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;
        final String? accessToken = Provider.of<UserProvider>(context).accessToken;

        Widget rowTaskInfo = Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        // Modify both the main and cross axis alignment.
                        children: [
                            Text(widget._task.title,style: TextStyle(fontSize: 20),),
                            Text(widget._task.description),
                        ]
                    )
                ),
                Chip(label: Text(widget._task.status!))
            ]
        );

        Widget btnStart = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Start'),
                onPressed: () {
                    API(accessToken).updateTaskStatus(
                        taskId: widget._task.id, 
                        status: "Ongoing"
                    ).catchError((error){
                        showSnackBar(context, error.toString());
                    });
                    widget._reloadTasks();
                }
            )
        );

        Widget btnFinish = Container(
            margin: EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
                child: Text('Finish'),
                onPressed: () {
                    API(accessToken).updateTaskStatus(
                        taskId: widget._task.id, 
                        status: "Completed"
                    ).catchError((error){
                        showSnackBar(context, error.toString());
                    });
                    widget._reloadTasks();
                },
            )
        );

        Widget btnAccept = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Accept'),
                 onPressed: () {
                    API(accessToken).updateTaskStatus(
                        taskId: widget._task.id, 
                        status: "Accepted"
                    ).catchError((error){
                        showSnackBar(context, error.toString());
                    });
                    widget._reloadTasks();
                },
            )
        );

        Widget btnReject = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Reject'),
                 onPressed: () {
                    API(accessToken).updateTaskStatus(
                        taskId: widget._task.id, 
                        status: "Rejected"
                    ).catchError((error){
                        showSnackBar(context, error.toString());
                    });
                    widget._reloadTasks();
                },
            )
        );

        Widget btnDetail = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: GestureDetector(
                child: Text('Detail', 
                        style: TextStyle( 
                            fontWeight: FontWeight.w700,
                            decoration: TextDecoration.underline
                        )
                    ),
                onTap: () async {
                    await Navigator.push(context, MaterialPageRoute(builder: (context) => TaskDetailScreen(widget._task)));
                    widget._reloadTasks();
                },
            )
        );

        Widget rowActions = Row(
            children: [
                btnDetail,
                Spacer(),   
                designation.toString() == "assembly-team" && widget._task.status == "Pending" ? btnStart : Container(),
                designation.toString() == "assembly-team" && widget._task.status == "Ongoing" ? btnFinish : Container(),
                designation.toString() == "contractor" && (widget._task.status.toString() == "Completed" || widget._task.status.toString() == "Rejected") ? btnAccept: Container(),
                designation.toString() == "contractor" && (widget._task.status.toString() == "Completed" || widget._task.status.toString() == "Accepted") ? btnReject : Container(),
                // Show btnStart if designation is assembly team and task status is pending, else show an empty container.
                // Show btnFinish if designation is assembly team and task status is ongoing, else show an empty container.
                // Show btnAccept if designation is contractor and task status is completed or rejected, else show an empty container.
                // Show btnReject if designation is contractor and task status is completed or accepted, else show an empty container.
            ]
        );

        return Card(
            child: Padding(
                padding: EdgeInsets.all(16.0), 
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        rowTaskInfo,
                        SizedBox(height: 16.0),
                        rowActions
                    ]
                )
            )
        );
    }
}