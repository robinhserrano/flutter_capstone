import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class InterestPage extends StatelessWidget {
    @override
    Widget build(BuildContext context) {

        Widget carouselBox(String imagePath, String title){ 
            return Container(
                margin: EdgeInsets.only(top: 16),
                height: 500,
                width: 350,
                child: Column(children: [
                    Image.asset(imagePath, fit: BoxFit.contain),
                    Text(title, style: GoogleFonts.bebasNeue(fontSize: 23, color: Colors.white.withOpacity(0.92)),)
                    ]
                ),
            );
        }
        
        return Scaffold(
        body: Container(
            color:Color.fromRGBO(58,50,80,1),
            child: Column(children: [  
                CarouselSlider(items: <Widget>[
                    carouselBox("assets/ZZ1.jpg", "FIGHTING GAMES"),
                    carouselBox("assets/ZZ2.jpg", "ROLE PLAYING GAMES"),
                    carouselBox("assets/ZZ3.jpeg", "MARVEL"),
                    carouselBox("assets/ZZ4.jpeg", "ANIME"),
                ],
                options: CarouselOptions(
                    autoPlay: true
                    )
                ),
                Expanded(child: Container(
                    padding: EdgeInsets.symmetric(vertical: 50, horizontal: 25),
                    decoration:  BoxDecoration(
                        color: Color.fromRGBO(114,137,218,1),
                        borderRadius: BorderRadius.only(
                           topLeft: Radius.circular(60),
                           topRight: Radius.circular(60),
                        ),
                    ),
                    child: AnimatedTextKit(
                        displayFullTextOnTap: true,
                        animatedTexts: [
                            TypewriterAnimatedText("My Interest are playing video games such as Final Fantasy, Tekken, God of War, Gundam Versus among others. I enjoy wathing varoius Anime, Movies, and TV Series. I also like to build Gundam Model Kits in my spare time.",
                            textStyle: GoogleFonts.bebasNeue(fontSize: 30, color: Colors.white.withOpacity(0.92)),
                            textAlign: TextAlign.center)
                            ],
                            isRepeatingAnimation: false,
                        ),
                    ))
                ]),
            ));
        }
    }