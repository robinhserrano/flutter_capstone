import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_fonts/google_fonts.dart';

class IntroPage extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Container(
            child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                    Observer(builder: (_) {
                        return Container(
                            child: Image.asset(
                                "assets/planets.gif",
                                fit: BoxFit.cover,
                                ),
                            );
                        }
                    ),
                    Container(
                        width: 100,
                        margin: EdgeInsets.only(top: 100),
                        alignment: Alignment.topCenter,
                        child:  Column(
                            children: [
                                CircleAvatar(
                                    backgroundImage: AssetImage('assets/dp.png'),
                                    radius: 80.0,
                                )
                            ],
                        ),
                    ),
                    Container(
                        alignment: Alignment.bottomCenter,
                        margin: EdgeInsets.only(bottom: 100),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                                Text("Hello World!\n I'm Robin Andrei",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.lobster(
                                    color: Colors.white.withOpacity(0.92),
                                    fontSize: 36)),
                                    SizedBox(height: 30,),
                                ],
                            )
                        )
                    ],
                ),
            );
        }
    }

   