import 'package:flutter/material.dart';

import 'about_pages/about_contacts.dart';
import 'about_pages/about_interests.dart';
import 'about_pages/about_intro_page.dart';
import 'about_pages/about_short_intro.dart';
import 'about_pages/about_skills.dart';

class AboutScreen extends StatefulWidget {
  const AboutScreen({ Key? key }) : super(key: key);

  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: Center(
                child: PageView(
                    scrollDirection: Axis.vertical,
                    children: [
                        IntroPage(),
                        ShortIntroPage(),
                        SkillsPage(),
                        InterestPage(),
                        ContactPage()
                        ],
                    ),
                ),
            );
        }
    }




